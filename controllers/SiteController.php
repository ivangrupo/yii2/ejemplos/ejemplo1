<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Noticias;



class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        
        /**
         * La mejor forma de realizar consultas (sobre todo las consultas tipicas)
         * es mediante Active Record
         * Para ello vamos a utilizar la clase yii/db/ActiveRecord
         * Ademas tenemos que modelar todas las tablas
         * para realizar los modelos utilizamos Gii
         * 
         */
        
        $consulta= \app\models\Noticias::find();
        
        /**
         * Debido a que yii\db\ActiveQuery se extiende desde yii\db\Query , puede usar todos los métodos de creación de consultas y métodos de consulta del padre
         */
        
        $salidaArrayObjetos=$consulta->all();
       
            
        return $this->render('index',[
            'noticias'=>$salidaArrayObjetos,
        ]);
    }
    
    public function actionPagina1(){
        /**
         * Tengo dos opciones (hay mas pero hablemos de las principales)
         */
        
        /**
         * Realizo la consulta en controlador
         * 
         */
        
        //$total=Noticias::find()->count();
        //$n= Noticias::find()->offset(rand(0, $total-1))->limit(1)->one();
        
        /**
         * Realizo la consulta en el modelo y lo llamos desde el controlador
         * mejor metodo
         */
        
        $registro= Noticias::mostrarUna();
        
        return $this->render("pagina1",[
            "dato"=>$registro,
        ]);
    }
    
    public function actionPagina2(){
        return $this->render("pagina2");
    }

}
