<div>
    <?php
        echo \yii\helpers\Html::img("@web/imgs/f1.jpg",[
            "class"=>"img-responsive center-block img-circle",
            "alt"=>"Ejemplo de foto",
        ]);
    ?>
</div>
<div class="jumbotron noticias">
  <div class="container">
    <h1><?= $dato->titulo; ?></h1>
    <p><?= $dato->texto; ?></p>
  </div>
</div>

